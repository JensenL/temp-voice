from typing import List

import nox

PROD_DEPENDENCIES = [
    'discord.py==1.6.0',
    'loguru==0.5.3'
]

DEV_DEPENDENCIES = PROD_DEPENDENCIES + [
    'nox',
    'discord.py-stubs'
]

LINT_DEPENDENCIES = DEV_DEPENDENCIES + [
    'prospector[with_mypy]'
]


@nox.session(reuse_venv=True, python='3.8')
def dev(session):
    install_dependencies(session, DEV_DEPENDENCIES)


@nox.session(reuse_venv=True, python='3.8')
def prod(session):
    install_dependencies(session, PROD_DEPENDENCIES)
    session.run('pip', 'freeze', '>', 'requirements.txt')


@nox.session(reuse_venv=True, python='3.8')
def lint(session):
    install_dependencies(session, LINT_DEPENDENCIES)
    session.run('prospector')


def install_dependencies(session, dependencies: List[str]):
    for dependency in dependencies:
        session.install(dependency)
