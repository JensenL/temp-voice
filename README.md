## Temp Voice
This Discord bot can create simple voice channels that are accompanied by a private text channel.
The text channels will stay hidden until one joins the matching voice channel.

## How to install
Clone the repository first:
> git clone https://gitlab.com/JensenL/temp-voice.git

Install dependencies with:
> pip install -r requirements.txt

Start the bot with:
> python main.py [YOUR BOT TOKEN]

Invite your bot with the link:
> https://discord.com/oauth2/authorize?client_id=[YOUR_CLIENT_ID_GOES_HERE]&scope=bot&permissions=285212688

## Usage
Simply create a voice channel called "!Join to create" in a category of your choice.
Any user that joins the created voice channel will trigger the bot to create a temporary channel-pair.
If such a tempory voice channel gets empty, the bot will delete all relevant channels.
