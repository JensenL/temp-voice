import argparse

from discord import LoginFailure
from loguru import logger

from bot.client import BotClient


@logger.catch()
def main(token: str):
    client = BotClient()
    try:
        client.run(token)
    except LoginFailure as error:
        logger.error('Could not start bot: {}', error)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Start the bot')
    parser.add_argument('token', type=str, help='Secret token of the bot')

    logger.add('temp-voice.log', rotation="10 MB", retention=2)
    args = parser.parse_args()
    main(args.token)
