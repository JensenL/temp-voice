import re
from typing import List, Union

from discord import Guild, Member, VoiceChannel, TextChannel
from discord.abc import GuildChannel
from loguru import logger


@logger.catch(reraise=True)
async def create_voice_pair(guild: Guild, channel: VoiceChannel, user: Member, _bot_member: Member):
    new_channel_name = __apply_text_channel_name_restrictions(f'{user}-new-temp')
    channel_number = 1
    while any(voice_channel.category == channel.category and voice_channel.name.lower() == new_channel_name.lower()
              for voice_channel in guild.voice_channels):
        new_channel_name = __apply_text_channel_name_restrictions(f'{user}-{channel_number}-new-temp')
        channel_number += 1

    # channel_permission_overwrite: Dict[Union[Role, Member], PermissionOverwrite] = {
    #     bot_member: PermissionOverwrite(read_messages=True),
    #     guild.default_role: PermissionOverwrite(read_messages=False)
    # }

    # new_text_channel: TextChannel = await guild.create_text_channel(
    #     new_channel_name,
    #     category=channel.category,
    #     overwrites=channel_permission_overwrite
    # )
    new_voice_channel = await guild.create_voice_channel(
        new_channel_name,
        category=channel.category,
        bitrate=int(guild.bitrate_limit)
    )
    await user.move_to(new_voice_channel)


@logger.catch(reraise=True)
async def clear_voice_pair(guild: Guild, channel: VoiceChannel):
    matches: List[GuildChannel] = [guild_channel for guild_channel in guild.channels
                                   if guild_channel.category == channel.category and
                                   guild_channel.name.lower() == channel.name.lower()]
    for match in matches:
        await match.delete(reason='Voice channel was empty')


@logger.catch(reraise=True)
async def add_text_permission(guild: Guild, channel: VoiceChannel, user: Member):
    matches = [guild_channel for guild_channel in guild.text_channels
               if guild_channel.category == channel.category and guild_channel.name.lower() == channel.name.lower()]
    if matches:
        target_channel = matches[0]
        await target_channel.set_permissions(user, read_messages=True)


@logger.catch(reraise=True)
async def remove_text_permission(guild: Guild, channel: VoiceChannel, user: Member):
    matches = [guild_channel for guild_channel in guild.text_channels
               if guild_channel.category == channel.category and guild_channel.name.lower() == channel.name.lower()]
    if matches:
        target_channel = matches[0]
        await target_channel.set_permissions(user, overwrite=None)


@logger.catch(reraise=True)
async def rename_voice_pair(guild: Guild, channel: Union[TextChannel, VoiceChannel], new_name: str):
    if channel.name.endswith('-new-temp'):
        if new_name.endswith('-new'):
            new_name = new_name[:-4]
        new_name = __apply_text_channel_name_restrictions(f'{new_name}-temp')
        new_name = re.sub('-+', '-', new_name)
        if new_name == '-temp':
            await send_to_channel(channel, 'Cant\'t rename channel, as new name would be empty.')
            return

        if any(existing_channel.name == new_name and existing_channel.category == channel.category
               for existing_channel in guild.channels):
            await send_to_channel(channel, 'Name is already used in this category, please use a different name.')
            return

        matching_channels = [guild_channel for guild_channel in guild.channels
                             if guild_channel.category == channel.category and
                             guild_channel.name.lower() == channel.name.lower()]

        for match in matching_channels:
            await match.edit(name=new_name)

    elif channel.name.endswith('-temp'):
        await send_to_channel(
            channel,
            'This channel was already renamed and cannot be renamed twice due to Discord\'s API rate limit.'
        )


async def send_to_channel(channel: Union[TextChannel, VoiceChannel], message: str):
    if isinstance(channel, TextChannel):
        await channel.send(message)


def __apply_text_channel_name_restrictions(input_string) -> str:
    cleaned_string = re.sub('[^A-Za-z0-9-_°]', '', input_string.replace(' ', '-'))
    return re.sub('-+', '-', cleaned_string)
