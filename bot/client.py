import discord
from discord import VoiceState, Member, VoiceChannel, Intents, Message, ChannelType, TextChannel
from discord.utils import oauth_url
from loguru import logger

from bot.skills.manage_voice_text import create_voice_pair, add_text_permission, remove_text_permission, \
    clear_voice_pair, rename_voice_pair


class BotClient(discord.Client):

    def __init__(self, **options):
        intents = Intents.default()
        intents.voice_states = True
        intents.guild_messages = True
        super().__init__(intents=intents, **options)

    async def on_ready(self):
        logger.info('Logged on as {}', self.user)
        logger.info('You can invite the bot by using the following url: {}',
                    oauth_url(self.user.id, permissions=discord.Permissions(permissions=285212688)))
        await self.change_presence(activity=(discord.Game("!Join to create")))

    async def on_voice_state_update(self, member: Member, before: VoiceState, after: VoiceState):
        if after.channel != before.channel:
            if isinstance(after.channel, VoiceChannel):
                if after.channel.name == '!Join to create':
                    bot_member = member.guild.get_member(self.user.id)
                    assert bot_member
                    await create_voice_pair(member.guild, after.channel, member, bot_member)
                if after.channel.name.endswith('-temp'):
                    await add_text_permission(member.guild, after.channel, member)

            if isinstance(before.channel, VoiceChannel):
                if before.channel.name.endswith('-temp'):
                    await remove_text_permission(member.guild, before.channel, member)
                if before.channel.name.endswith('-temp') and len(before.channel.members) == 0:
                    await clear_voice_pair(member.guild, before.channel)

    async def on_message(self, message: Message):
        if message.author == self.user:
            return
        if message.content.startswith('!rename '):
            new_name = message.content[len('!rename '):]
            channel = message.channel
            guild = message.guild
            if guild and (channel.type == ChannelType.text or channel.type == ChannelType.voice):
                assert isinstance(channel, TextChannel) or isinstance(channel, VoiceChannel)
                await rename_voice_pair(guild, channel, new_name)
